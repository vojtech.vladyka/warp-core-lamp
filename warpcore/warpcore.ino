#include <Adafruit_NeoPixel.h>
#include <math.h>

#define PIN 6
#define BUTTON 2
#define PIXELS 70

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXELS, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  pinMode(BUTTON, INPUT_PULLUP);

  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

void loop() {
  static uint8_t effect = 0;
  static uint8_t did_change = 0;

  uint8_t buttonState = digitalRead(BUTTON);

  if (buttonState == HIGH) {
    did_change = 0;
  } else {
    if(did_change == 0) {
      effect++;
      did_change = 1;
    }
  }

  switch(effect) {
    case 0:
      warp_enterprise_d(10, 10, 100, 0, 0, 10, 2.5, 20.0); //standby
      break;
    case 1:
      warp_enterprise_d(50, 50, 130, 10, 10, 30, 2.0, 8.0); //normal run
      break;
    case 2:
      warp_enterprise_d(100, 50, 130, 20, 10, 30, 1.3, 2.0); //haste
      break;
    case 3:
      warp_voyager(20, 20, 50, 4, 4, 20, 5.0, 5.0, 2.0, 23.0, 23.0, -23.0); //standby
      break;
    case 4:
      warp_voyager(120, 90, 130, 4, 4, 90, 5.0, 5.0, 2.0, 13.0, 13.0, -13.0); //normal run
      break;
    case 5:
      warp_voyager(140, 90, 130, 4, 4, 90, 5.0, 5.0, 2.0, 3.0, 3.0, -3.0); //haste
      break;
    case 6:
      glower(140, 90, 90, 40, 40, 40, 5.0, 5.0, 5.0, 30.0, 30.0, 30.0); //white
      break;
    case 7:
      glower(140, 50, 30, 40, 20, 20, 5.0, 5.0, 5.0, 50.0, 50.0, 50.0); //yelowish
      break;
    case 8:
      colour(140, 60, 40);
      break;
    case 9:
      colour(140, 0, 0);
      break;
    case 10:
      colour(140, 70, 0);
      break;
    case 11:
      colour(140, 140, 0);
      break;
    case 12:
      colour(70, 140, 0);
      break;
    case 13:
      colour(0, 140, 0);
      break;
    case 14:
      colour(0, 140, 70);
      break;
    case 15:
      colour(0, 140, 140);
      break;
    case 16:
      colour(0, 70, 140);
      break;
    case 17:
      colour(0, 0, 140);
      break;
    case 18:
      colour(70, 0, 140);
      break;
    case 19:
      colour(140, 0, 140);
      break;
    case 20:
      colour(140, 0, 70);
      break;
    case 21:
      rainbow(20);
      break;
    case 22:
      colour(0, 0, 0);
      break;
    default:
      effect = 0;
  }
}

uint8_t __phaser(int16_t phase, uint8_t amplitude, uint8_t bias, float offset) {
  int32_t p = (amplitude)*sin(phase)+(amplitude/offset);
  p = p > 0 ? p : bias;
  return p;
}

void colour(uint8_t amp_r, uint8_t amp_g, uint8_t amp_b)  {

  for(uint16_t i=0; i<strip.numPixels(); i++) {
    uint32_t colour = strip.Color(amp_r, amp_g, amp_b);
    strip.setPixelColor(i, colour);
  }
  strip.show();
}

void glower(uint8_t amp_r, uint8_t amp_g, uint8_t amp_b, uint8_t off_r, uint8_t off_g, uint8_t off_b, float width_r, float width_g, float width_b, float speed_r, float speed_g, float speed_b)  {

  static uint16_t t=0;
  for(uint16_t i=0, j=strip.numPixels()/2; i<strip.numPixels()/4; i++, j--) {
    uint8_t red = __phaser(i/width_r - t/speed_r, amp_r, off_r, 2);
    uint8_t green = __phaser(i/width_g -t/speed_g, amp_g, off_g, 2);
    uint8_t blue = __phaser(i/width_b - t/speed_b, amp_b, off_b, 2);
    
    uint32_t colour = strip.Color(red, green, blue);

    strip.setPixelColor(i, colour);
    strip.setPixelColor(j, colour);
    strip.setPixelColor(strip.numPixels()/2+i, colour);
    strip.setPixelColor(strip.numPixels()/2+j, colour);
  }
  strip.show();
  delay(5);
  t++;
}

void warp_voyager(uint8_t amp_r, uint8_t amp_g, uint8_t amp_b, uint8_t off_r, uint8_t off_g, uint8_t off_b, float width_r, float width_g, float width_b, float speed_r, float speed_g, float speed_b)  {

  static uint16_t t=0;
  for(uint16_t i=0, j=strip.numPixels()/2; i<strip.numPixels()/4; i++, j--) {
    uint8_t red = __phaser(i/width_r - t/speed_r, amp_r, off_r, 2);
    uint8_t green = __phaser(i/width_g -t/speed_g, amp_g, off_g, 2);
    uint8_t blue = __phaser(i/width_b - t/speed_b, amp_b, off_b, 2);
    
    uint32_t colour = strip.Color(red, green, blue);

    strip.setPixelColor(i, colour);
    strip.setPixelColor(j, colour);
    strip.setPixelColor(strip.numPixels()/2+i, colour);
    strip.setPixelColor(strip.numPixels()/2+j, colour);
  }
  strip.show();
  delay(10);
  t++;
}

void warp_enterprise_d(uint8_t amp_r, uint8_t amp_g, uint8_t amp_b, uint8_t off_r, uint8_t off_g, uint8_t off_b, float width, float speed)  {

  static uint16_t t=0;
  for(uint16_t i=0, j=strip.numPixels()/2; i<strip.numPixels()/4; i++, j--) {
    uint8_t red = __phaser(i/width-t/speed, amp_r, off_r, 2);
    uint8_t green = __phaser(i/width-t/speed, amp_g, off_g, 2);
    uint8_t blue = __phaser(i/width-t/speed, amp_b, off_b, 2);
    
    uint32_t colour = strip.Color(red, green, blue);

    strip.setPixelColor(i, colour);
    strip.setPixelColor(j, colour);
    strip.setPixelColor(strip.numPixels()/2+i, colour);
    strip.setPixelColor(strip.numPixels()/2+j, colour);
  }
  strip.show();
  delay(10);
  t++;
}

void rainbow(int wait) {
  static long firstPixelHue = 0; 
  if(firstPixelHue >= 5*65536) {
    firstPixelHue = 0;
  }
  for(int i=0; i<strip.numPixels(); i++) { 
    int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());
    strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
  }
  strip.show(); 
  delay(wait); 
  firstPixelHue += 256;
}


